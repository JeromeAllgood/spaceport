from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import (
    Flight, Passenger, Planet, Port, ShipModel, Ship, Tag, Ticket, Route,
    User, UserFeedback)


# Register your models here.
@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)


@admin.register(Planet)
class PlanetAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'coordinates_hgi')
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Port)
class PortAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'capacity', 'planet', 'coordinate_lat',
                    'coordinate_lon')
    prepopulated_fields = {'slug': ('name',)}


@admin.register(ShipModel)
class ShipModelAdmin(admin.ModelAdmin):
    list_display = ('ship_type', 'capacity', 'hyper_drive')


@admin.register(Ship)
class ShipAdmin(admin.ModelAdmin):
    list_display = ('serial_number', 'model', 'color', 'tech_check_date',
                    'exploitation_start_date')


@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = ('route_name', 'departure_port', 'destination_port',
                    'trip_time_sol')


class TagInline(admin.TabularInline):
    model = Tag.flight.through


@admin.register(Flight)
class FlightAdmin(admin.ModelAdmin):
    list_display = ('route', 'date', 'ship', 'status', 'available_tickets')
    inlines = [
        TagInline,
    ]


@admin.register(Passenger)
class PassengerAdmin(admin.ModelAdmin):
    list_display = ('user', 'passport_id', 'address',
                    'phone', 'health_profile', 'card')


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_display = ('flight', 'passenger')


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('tag', 'slug')
    prepopulated_fields = {'slug': ('tag',)}


@admin.register(UserFeedback)
class UserFeedbackAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'message', 'timestamp')
