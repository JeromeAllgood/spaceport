from django.apps import AppConfig


class SpaseportAppConfig(AppConfig):
    name = 'spaseport_app'
