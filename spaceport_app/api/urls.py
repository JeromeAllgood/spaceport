from django.urls import path

from . import views

urlpatterns = [
    path('ships/', views.ShipListView.as_view()),
    path('ships/<int:pk>/', views.ShipDetailView.as_view()),
    path('models/', views.ModelListView.as_view()),
    path('models/<int:pk>/', views.ModelDetailView.as_view()),
    path('users/', views.UserListView.as_view()),
    path('passengers/', views.PassengerListView.as_view()),

]
