from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """User model."""

    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()


class Planet(models.Model):
    name = models.CharField(max_length=15)
    slug = models.SlugField(max_length=15)
    coordinates_hgi = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.name


class Port(models.Model):
    planet = models.ForeignKey(Planet, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    coordinate_lat = models.CharField(max_length=20)
    coordinate_lon = models.CharField(max_length=20)
    capacity = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.name} ({self.planet})"


class ShipModel(models.Model):
    ship_type = models.CharField(max_length=20)
    capacity = models.PositiveIntegerField()
    hyper_drive = models.BooleanField(default=False)

    def __str__(self):
        return self.ship_type


class Ship(models.Model):
    serial_number = models.CharField(max_length=10)
    color = models.CharField(max_length=50)
    tech_check_date = models.DateField()
    exploitation_start_date = models.DateField()
    model = models.ForeignKey(ShipModel, related_name='ships',
                              related_query_name="ship",
                              on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.serial_number} - {self.model}'

    @property
    def model_ship_type(self):
        return self.model.ship_type

    @property
    def capacity(self):
        return self.model.capacity


class Route(models.Model):
    route_name = models.CharField(max_length=15)
    trip_time_sol = models.DecimalField(max_digits=7, decimal_places=2)
    departure_port = models.ForeignKey(Port, on_delete=models.CASCADE,
                                       related_name='departure')
    destination_port = models.ForeignKey(Port, on_delete=models.CASCADE,
                                         related_name='destination')

    def __str__(self):
        return f'{self.route_name} {self.departure_port} - ' \
               f'{self.destination_port}'


class Flight(models.Model):
    STATUS_CHOICES = (
        ('planned', 'Planned'),
        ('ready', 'Ready'),
        ('in flight', 'In flight'),
        ('completed', 'Completed')
    )

    date = models.DateTimeField()
    ship = models.ForeignKey(Ship, on_delete=models.CASCADE)
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES,
                              default='planned')
    available_tickets = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return f'{self.route} - {self.date}'

    def save(self, *args, **kwargs):
        # get capacity
        # set available seats = capacity if id == None
        if not self.id:
            self.available_tickets = self.ship.model.capacity
        super().save(*args, **kwargs)


class Passenger(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                related_name='passengers', null=True)
    health_profile = models.BooleanField(default=False)
    address = models.CharField(max_length=200, blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)
    passport_id = models.CharField(max_length=10, blank=True, null=True)
    card = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'


class Ticket(models.Model):
    passenger = models.ForeignKey(Passenger, on_delete=models.CASCADE)
    flight = models.ForeignKey(Flight, on_delete=models.CASCADE)

    def __str__(self):
        return f'Ticket #{self.id}'

    def save(self, *args, **kwargs):
        if not self.id:
            self.flight.available_tickets -= 1
            self.flight.save()
        super().save(*args, **kwargs)

    def buy(self):
        self.clean()
        self.save()

    def validate_passenger(self):
        errors = []
        if not self.passenger.card:
            errors.append(ValidationError(
                _('Payment information not found.'),
                code='required payment info'))
        if not self.passenger.health_profile:
            errors.append(ValidationError(
                _('Health profile should be verified'),
                code='required health info'))
        if not self.passenger.passport_id:
            errors.append(ValidationError(_('Passport/ID should be legit.'),
                                          code='required ID info'))
        if errors:
            raise ValidationError(errors)

    def validate_flight(self):
        errors = []
        if self.flight.date < timezone.now():
            errors.append(ValidationError(_('This flight has started.'),
                                          code='invalid flight date'))
        if self.flight.available_tickets < 1:
            errors.append(ValidationError(_('Sold out!'), code='no tickets'))
        if errors:
            raise ValidationError(errors)

    def clean(self):
        self.validate_flight()
        self.validate_passenger()


class Tag(models.Model):
    tag = models.CharField(max_length=100, unique=True)
    flight = models.ManyToManyField(Flight)
    slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.tag


class UserFeedback(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    message = models.TextField()
    timestamp = models.DateTimeField()
