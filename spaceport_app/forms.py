from django import forms
from django.contrib.auth.models import User

from .models import Passenger


class SignupForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter email'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter password'}))


class LoginForm(forms.Form):
    user_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter username'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Enter password'}))


class PassengerForm(forms.ModelForm):
    class Meta:
        model = Passenger
        fields = ['health_profile', 'address',
                  'phone', 'passport_id', 'card']
        widgets = {
            'health_profile': forms.CheckboxInput(
                attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'passport_id': forms.TextInput(attrs={'class': 'form-control'}),
            'card': forms.TextInput(attrs={'class': 'form-control'})
        }


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name']
        widgets = {
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'})
        }


class SearchForBuyForm(forms.Form):
    departure = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Departure',
               'id': 'departure'}))
    destination = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Destination',
               'id': 'destination'}))


class ContactForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your name'
    }))
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your email'
    }))
    message = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your message'
    }))
