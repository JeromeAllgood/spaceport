from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path('', views.base_view, name='base-view'),
    path('planets/', views.planet_list, name='planets'),
    path('planets/<slug:slug>/', views.planet_detail, name='planet-detail'),
    path('ports/<slug:slug>/', views.port_detail, name='port-detail'),
    path('routes/', views.route_list, name='routes'),
    path('tags/', views.tag_list, name='tags'),
    path('tags/<slug:slug>/', views.tag_detail, name='tag-detai;'),
    path('sign-up/', views.sign_up, name='sign-up'),
    path('search-result/', views.search, name='search'),
    path('profile/', views.profile, name='profile'),
    path('profile/update-profile/<int:user_id>', views.update_profile,
         name='update-profile'),
    path('login/', views.user_login, name='login'),
    path('logout/', views.user_logout, name='logout'),
    path('flight-archive/', views.flight_archive, name='flight-archive'),
    path('profile/my-tickets/', views.my_tickets, name='my-tickets'),
    path('buy-ticket/', views.buy_ticket, name='buy-tickets'),
    path('ships/', views.ShipListView.as_view(), name='ships'),
    path('ships/<int:pk>/', views.ShipDetailView.as_view(), name='ship-detail'),
    path('models/<int:pk>/', views.ModelDetailView.as_view(),
         name='model-detail'),
    path('models/', views.ModelLstView.as_view(), name='models'),
    path('contact-us/', views.ContactUs.as_view(), name='contact-us'),
    path('thank-you/', views.ThankYou.as_view(), name='thank-you'),

]
