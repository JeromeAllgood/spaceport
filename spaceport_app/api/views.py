from ..models import Ship, ShipModel, Passenger, User
from . import serializers
from rest_framework import generics, status


class ShipListView(generics.ListAPIView):
    queryset = Ship.objects.all()
    serializer_class = serializers.ShipSerializer


class ShipDetailView(generics.RetrieveAPIView):
    queryset = Ship.objects.all()
    serializer_class = serializers.ShipSerializer


class ModelDetailView(generics.RetrieveAPIView):
    queryset = ShipModel.objects.all()
    serializer_class = serializers.ShipModelSerializer


class ModelListView(generics.ListAPIView):
    queryset = ShipModel.objects.all()
    serializer_class = serializers.ShipModelSerializer


class CreateShipModelView(generics.CreateAPIView):
    serializer_class = serializers.ShipModelSerializer


class PassengerListView(generics.ListAPIView):
    queryset = Passenger.objects.all()
    serializer_class = serializers.PassengerSerializer


class UserListView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
