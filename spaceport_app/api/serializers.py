from ..models import Ship, ShipModel, Passenger, User
from rest_framework import serializers


class ShipSerializer(serializers.ModelSerializer):
    model_ship_type = serializers.ReadOnlyField()
    capacity = serializers.ReadOnlyField()

    class Meta:
        model = Ship
        fields = (
            'id', 'model_ship_type', 'serial_number', 'capacity', 'color',
            'tech_check_date', 'exploitation_start_date')


class ShipModelSerializer(serializers.ModelSerializer):
    ships = ShipSerializer(many=True, read_only=True)

    class Meta:
        model = ShipModel
        fields = ('id', 'ship_type', 'capacity', 'hyper_drive', 'ships')


class PassengerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passenger
        fields = (
            'id', 'user', 'health_profile', 'address', 'phone', 'passport_id',
            'card')


class UserSerializer(serializers.ModelSerializer):
    passengers = PassengerSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'passengers')
