from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.views.generic import ListView, FormView, DetailView
from django.views.generic.base import TemplateView

from .forms import (
    ContactForm, LoginForm, PassengerForm, SearchForBuyForm, SignupForm,
    UserForm
)
from .models import (
    Flight, Passenger, Planet, Port, Route, Tag, Ticket, Ship, ShipModel, User,
    UserFeedback)


def base_view(request):
    form = SearchForBuyForm()
    return render(request, 'base_redesign.html', {'form': form})


def planet_list(request):
    planets = Planet.objects.filter()
    return render(request, 'planet_list.html', {'planets': planets})


def route_list(request):
    routes = Route.objects.filter().order_by('departure_port')
    return render(request, 'route_list.html', {'routes': routes})


def planet_detail(request, slug):
    planet = get_object_or_404(Planet, slug=slug)
    ports = Port.objects.filter(planet=planet.id)
    return render(request, 'planet_detail.html', {
        'planet': planet,
        'ports': ports
    })


def port_detail(request, slug):
    port = Port.objects.filter(slug=slug).first()
    flights = Flight.objects.filter(route__departure_port=port)
    return render(request, 'port_detail.html', {
        'port': port,
        'flights': flights
    })


def tag_list(request):
    tags = Tag.objects.all()
    return render(request, 'tag_list.html', {'tags': tags})


def tag_detail(request, slug):
    tag = Tag.objects.filter(slug=slug).first()
    flights = Flight.objects.filter(tag=tag)
    return render(request, 'tag_detail.html', {'tag': tag, 'flights': flights})


class ShipListView(ListView):
    model = Ship
    template_name = 'ship_list.html'


class ShipDetailView(DetailView):
    queryset = Ship.objects.all()
    template_name = 'ship_detail.html'

    def get_object(self, queryset=None):
        obj = super().get_object()
        # Record the last accessed date
        obj.last_accessed = timezone.now()
        obj.save()
        return obj


class ModelDetailView(DetailView):
    queryset = ShipModel.objects.all()
    template_name = 'model_detail.html'

    def get_object(self, queryset=None):
        obj = super().get_object()
        # Record the last accessed date
        obj.last_accessed = timezone.now()
        obj.save()
        return obj


class ModelLstView(ListView):
    model = ShipModel
    template_name = 'models.html'


def profile(request):
    return render(request, 'profile.html')


def buy_ticket(request):
    if request.method == 'GET':
        departure = request.GET.get('departure')
        destination = request.GET.get('destination')
        flights = Flight.objects.filter(
            route__departure_port__planet__name__icontains=departure).filter(
            route__destination_port__planet__name__icontains=destination).filter(
            date__gt=timezone.now()).filter(available_tickets__gt=0).order_by(
            'date')
        return render(request, 'buy_ticket.html',
                      {'dep': departure, 'dest': destination,
                       'flights': flights})
    else:
        passenger = Passenger.objects.get(user=request.user)
        flight = Flight.objects.get(id=request.POST['flight'])
        new_ticket = Ticket(
            flight=flight,
            passenger=passenger
        )
        try:
            new_ticket.buy()
        except ValidationError as e:
            dep = request.GET.get('departure')
            dest = request.GET.get('destination')
            flights = Flight.objects.filter(
                route__departure_port__planet__name__icontains=dep).filter(
                route__destination_port__planet__name__icontains=dest).filter(
                date__gt=timezone.now()).filter(
                available_tickets__gt=0).order_by(
                'date')
            return render(request, 'buy_ticket.html',
                          {'flights': flights, 'error': e})
        return render(request, 'success_tickets.html', {'ticket': new_ticket})


def search(request):
    query = request.GET.get('flight')
    flights = Flight.objects.filter(
        Q(route__route_name__icontains=query) | Q(
            route__departure_port__name__icontains=query
        )
    ).order_by('date')
    return render(request, 'search_result.html',
                  {'flights': flights, 'query': query})


def sign_up(request):
    if request.method == 'GET':
        form = SignupForm()
    else:
        form = SignupForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            try:
                user = User.objects.create_user(**cd)
                user.save()
                passenger = Passenger.objects.create(user=user)
                passenger.save()
                login(request, user)
            except IntegrityError as i:
                return render(request, 'sign_up.html',
                              {'form': form, 'error': i})
        return render(request, 'success.html')
    return render(request, 'sign_up.html', {'form': form})


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(
                request,
                username=cd['user_name'],
                password=cd['password']
            )
            if user:
                login(request, user)
                return redirect('/buy-tickets/')
            else:
                return redirect('/login/')
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


def user_logout(request):
    logout(request)
    return redirect('/')


@login_required(login_url='/login/')
def update_profile(request, user_id):
    user = User.objects.get(id=user_id)
    if user != request.user:
        return redirect(f'/profile/update-profile/{request.user.id}')
    passenger = Passenger.objects.get(user_id=user.id)
    if request.method == 'GET':
        user_form = UserForm(instance=user)
        passenger_form = PassengerForm(instance=passenger)
        return render(request, 'update_profile.html', {
            'user': user,
            'user_form': user_form,
            'passenger_form': passenger_form
        })
    else:
        user_form = UserForm(request.POST, instance=user)
        passenger_form = PassengerForm(request.POST, instance=passenger)
        if user_form.is_valid() and passenger_form.is_valid():
            user_form.save()
            passenger_form.save()
            return render(request, 'success.html')


def flight_archive(request):
    flights = Flight.objects.filter(date__lt=timezone.now()).order_by('date')
    return render(request, 'flight_archive.html', {'flights': flights})


def my_tickets(request):
    tickets = Ticket.objects.filter(passenger__user=request.user)
    return render(request, 'my_tickets.html', {'tickets': tickets})


class ContactUs(FormView):
    template_name = 'contact.html'
    form_class = ContactForm
    success_url = '/thank-you/'

    def form_valid(self, form):
        new_message = UserFeedback(**form.cleaned_data,
                                   timestamp=timezone.now())
        new_message.save()
        return super().form_valid(form)


class ThankYou(TemplateView):
    template_name = 'thank_you.html'
