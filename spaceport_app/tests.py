from django.test import TestCase
from django.urls import reverse, resolve

from . import views
from spaceport_app.models import Planet, User


class BaseTests(TestCase):
    def test_base_view_status_code(self):
        url = reverse('base-view')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_base_url_resolves_base_view(self):
        view = resolve('/')
        self.assertEquals(view.func, views.base_view)


class PlanetsTest(TestCase):
    def test_base_view_status_code(self):
        url = reverse('planets')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_planets_resolve_planets_view(self):
        view = resolve('/planets/')
        self.assertEquals(view.func, views.planet_list)


class PlanetsDetailTest(TestCase):
    def setUp(self):
        Planet.objects.create(name='TestPlanet', slug='test-planet',
                              coordinates_hgi=12.7)

    def test_planet_detail_status_code(self):
        url = reverse('planet-detail', kwargs={'slug': 'test-planet'})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_planet_detail_not_found_status_code(self):
        url = reverse('planet-detail', kwargs={'slug': 'new-planet'})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_planet_detail_resolve_planet_detail_view(self):
        view = resolve('/planets/test-planet/')
        self.assertEquals(view.func, views.planet_detail)


class RoutesTest(TestCase):
    def test_routes_status_code(self):
        url = reverse('routes')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_routes_resolve_routes_view(self):
        view = resolve('/routes/')
        self.assertEquals(view.func, views.route_list)


class TagsTest(TestCase):
    def test_base_view_status_code(self):
        url = reverse('tags')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_tags_resolve_tags_view(self):
        view = resolve('/tags/')
        self.assertEquals(view.func, views.tag_list)


class UserTest(TestCase):
    def setUp(self):
        User.objects.create(email='testuser@test.com', password='12345')

    def test_csrf(self):
        url = reverse('login')
        response = self.client.get(url)
        self.assertContains(response, 'csrfmiddlewaretoken')

    def test_new_user(self):
        url = reverse('sign-up')
        data = {
            'email': 'newuser@test.com',
            'password': 'new'
        }
        response = self.client.post(url, data)
        self.assertTrue(User.objects.filter(email=data['email']).exists())

    def test_new_user_invalid_post_data(self):
        url = reverse('sign-up')
        response = self.client.post(url, {})
        self.assertEquals(response.status_code, 200)
